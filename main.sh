#! /bin/bash

#define folders
#mkdir ~/documents ~/pictures

#install packages:Drivers,wm,tools, etc.
#sudo xbps-install -Sy linux-firmware-intel xorg vulkan-loader mesa-vulkan-intel intel-video-accel intel-media-driver pipewire i3-gaps rxvt-unicode urxvt-perls nitrogen firefox telegram-desktop rofi polybar flameshot psmisc python3 python3-pip python3-pipenv xsel xclip

#config .xinitrc
#make .xinitrc file
#cat <<EOT >> ~/.xinitrc
#pipewire &
#exec /bin/i3
#EOT

#config keyboard
#echo "setxkbmap us altgr-intl" >> ~/.bashrc  
#echo "setxkbmap us altgr-intl" >> ~/.bash_profile

#export dotfiles
#git clone https://gitlab.com/mcveight14/dotfiles.git ~/documents/dotfiles
#cp ~/documents/dotfiles/Xresources ~/.Xresources 
#cp ~/documents/dotfiles/Xresources ~/.Xdefaults 
#cp ~/documents/dotfiles/config ~/.config/i3/config

#config polybar
#git clone --depth=1 https://github.com/adi1090x/polybar-themes.git ~/polybar-themes
#cd ~/polybar-themes
#chmod +x setup.sh
#./setup.sh


